package cc.siyecao.uid.core.resposity;

import cc.siyecao.uid.core.utils.RandomUtils;

public class WorkerNodeDefault implements WorkerNodeResposity {

    @Override
    public WorkerNodeEntity getWorkerNodeByHostPort(String host, String port) {
        return null;
    }

    @Override
    public void addWorkerNode(WorkerNodeEntity entity) {
        entity.setId( RandomUtils.randomInt( 0, 1000 ) );
    }
}
