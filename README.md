# uid-spring-boot-starter

[![License](https://img.shields.io/badge/license-Apache%202-4EB1BA.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

#### 项目说明

UidGenerator是百度开源的基于Snowflake算法的唯一ID生成器，使用java语言实现，可在分布式环境下生成单调递增的ID。详情参见：
**[uid-generator](https://github.com/baidu/uid-generator/blob/master/README.zh_cn.md)**

从官网说明或者网上的使用教程可见，将其集成到springboot项目中，还是有点小麻烦的。此项目对uid-generator进行了springboot Starter风格的封装，只要一行注释便可将其集成到项目中，同时还增加一些实用的特性。

#### 新增的特性

1、spring-boot-starter风格的开箱即用。

2、可为uid-generator独立设置数据源，和业务系统的主数据源分开。

3、支持使用ZooKeeper进行WORKER ID分配，藉由ZK的Paxos强一致性算法获取更高的可用性。

4、支持使用数据库、redis进行WORKER ID分配。

**快速开始**

1、引入uid-spring-boot-starter

	<dependency>
		<groupId>cc.siyecao.uid</groupId>
		<artifactId>uid-spring-boot-starter</artifactId>
		<version>最新的版本号</version>
	</dependency>

2、引入db、redis、zk其中一种对应实现包，不引入则使用uuid默认实现

	<dependency>
		<groupId>cc.siyecao.uid</groupId>
		<artifactId>uid-redis</artifactId>
		<version>最新的版本号</version>
	</dependency>

	<dependency>
		<groupId>cc.siyecao.uid</groupId>
		<artifactId>uid-zookeeper</artifactId>
		<version>最新的版本号</version>
	</dependency>

	<dependency>
		<groupId>cc.siyecao.uid</groupId>
		<artifactId>uid-database</artifactId>
		<version>最新的版本号</version>
	</dependency>

3、使用

	@Resource
	private UidGenerator uidGenerator;
	
	@Test
	public void contextLoads()  {
		for(int i=0;i<100;i++) {
			System.out.println("uid:"+uidGenerator.getUID());
		}
	}

**使用独立的数据源**

在数据库中创建WORKER_NODE表，使用其作为uid-generator的专用数据库

```
DROP TABLE IF EXISTS WORKER_NODE;
CREATE TABLE WORKER_NODE
(
	ID BIGINT NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
	HOST_NAME VARCHAR(64) NOT NULL COMMENT 'host name',
	PORT VARCHAR(64) NOT NULL COMMENT 'port',
	TYPE INT NOT NULL COMMENT 'node type: ACTUAL or CONTAINER',
	LAUNCH_DATE DATE NOT NULL COMMENT 'launch date',
	MODIFIED TIMESTAMP NOT NULL COMMENT 'modified time',
	CREATED TIMESTAMP NOT NULL COMMENT 'created time',
	PRIMARY KEY(ID)
)
COMMENT='DB WorkerID Assigner for UID Generator',ENGINE = INNODB;
```

```
#---------------------- 业务配置   -----------------------
spring:
  datasource: #业务数据源
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/yewu1
    password: admin
    username: 123456
#---------------------- uid-generator   -----------------------
uid-generator: 
  #workerId获取方式：none：随机数获取；db：数据库；redis：redis（集群或单机）；zk：（zk集群或单机）
  assigner-mode: db
  #time-bits: 28 #可选配置, 如未指定将采用默认值
  #worker-bits: 22 #可选配置, 如未指定将采用默认值
  #seq-bits: 13 #可选配置, 如未指定将采用默认值
  #epoch-str: 2016-05-20 #可选配置, 如未指定将采用默认值
  #boost-power: 3 #可选配置, 如未指定将采用默认值
  #padding-factor: 50 #可选配置, 如未指定将采用默认值
  #schedule-interval:  #可选配置, 如未指定则不启用此功能
  datasource: #使用独立的数据源,如未指定将采用应用系统的数据源
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://192.168.1.666:3306/uid-db
    password: admin
    username: 123456
```

**使用zookeeper**

追求高可用推荐使用zookeeper集群模式

```
#---------------------- 业务配置   -----------------------
spring:
  datasource: #业务数据源
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/yewu?
    password: admin
    username: 123456
#---------------------- uid-generator   -----------------------
uid-generator: 
   #workerId获取方式：none：随机数获取；db：数据库；redis：redis（集群或单机）；zk：（zk集群或单机）
  assigner-mode: zk
  #time-bits: 28 #可选配置, 如未指定将采用默认值
  #worker-bits: 22 #可选配置, 如未指定将采用默认值
  #seq-bits: 13 #可选配置, 如未指定将采用默认值
  #epoch-str: 2016-05-20 #可选配置, 如未指定将采用默认值
  #boost-power: 3 #可选配置, 如未指定将采用默认值
  #padding-factor: 50 #可选配置, 如未指定将采用默认值
  #schedule-interval:  #可选配置, 如未指定则不启用此功能
  #datasource: #使用独立的数据源,如未指定将采用应用系统的数据源
    #driver-class-name: com.mysql.cj.jdbc.Driver
    #url: jdbc:mysql://192.168.1.666:3306/uid-db
    #password: root
    #username: root
  zookeeper: 
    #zk连接地址，集群模式则用逗号分开，如： 192.168.1.333:2181,192.168.1.555:2182,192.168.1.66:2183
    addrs: 192.168.1.333:2181 
    #authentication: admin:123456 #digest类型的访问秘钥，如：user:password，默认为不使用秘钥
```

**使用redis**

追求高可用推荐使用redis集群模式

```
#---------------------- 业务配置   -----------------------
spring:
  datasource: #业务数据源
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/yewu?
    password: admin
    username: 123456
#---------------------- uid-generator   -----------------------
uid-generator: 
  #workerId获取方式：none：随机数获取；db：数据库；redis：redis（集群或单机）；zk：（zk集群或单机）
  assigner-mode: redis
  #time-bits: 28 #可选配置, 如未指定将采用默认值
  #worker-bits: 22 #可选配置, 如未指定将采用默认值
  #seq-bits: 13 #可选配置, 如未指定将采用默认值
  #epoch-str: 2016-05-20 #可选配置, 如未指定将采用默认值
  #boost-power: 3 #可选配置, 如未指定将采用默认值
  #padding-factor: 50 #可选配置, 如未指定将采用默认值
  #schedule-interval:  #可选配置, 如未指定则不启用此功能
  #datasource: #使用独立的数据源,如未指定将采用应用系统的数据源
    #driver-class-name: com.mysql.cj.jdbc.Driver
    #url: jdbc:mysql://192.168.1.666:3306/uid-db
    #password: root
    #username: root
  #zookeeper: 
    #zk连接地址，集群模式则用逗号分开，如： 192.168.1.333:2181,192.168.1.555:2182,192.168.1.66:2183
    #addrs: 192.168.1.333:2181 
    #authentication: admin:123456 #digest类型的访问秘钥，如：user:password，默认为不使用秘钥
    #redis数据库默认使用db0
  redis:
    # 连接超时时间
    timeout: 5000
    database: 0
    #      password:
    port: 6379
    host: 127.0.0.1
    lettuce:
      # 连接池最大连接数（使用负值表示没有限制）
      pool:
        max-active: 3
        # 连接池中的最小空闲连接
        min-idle: 2
        # 连接池中的最大空闲连接
        max-idle: 3
        # 连接池最大阻塞等待时间（使用负值表示没有限制）
        max-wait: 10
        #在关闭客户端连接之前等待任务处理完成的最长时间，在这之后，无论任务是否执行完成，都会被执行器关闭，默认100ms
        shutdown-timeout: 100
    #集群模式
#    cluster：
#      nodes: 172.81.235.217:7001,172.81.235.217:7002,172.81.235.217:7003,172.81.235.217:7004,172.81.235.217:7005,172.81.235.217:7006
```
